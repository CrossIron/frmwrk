<?php

namespace Frmwrk\Layouts;

/**
 * Interface Layout
 * @package Frmwrk\Layouts
 */
interface Layout
{
    /**
     * The layout view's name
     * @return string
     */
    public function getName(): string;

    /**
     * The data to be injected into the layout
     * @return array
     */
    public function getData(): array;
}
