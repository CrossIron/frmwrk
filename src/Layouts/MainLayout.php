<?php

namespace Frmwrk\Layouts;

/**
 * Class MainLayout
 * @package Frmwrk\Layouts
 */
class MainLayout implements Layout
{
    /**
     * @var array
     */
    private $data = [];

    /**
     * MainLayout constructor.
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return 'layout::main';
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }
}