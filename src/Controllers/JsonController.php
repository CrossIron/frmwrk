<?php

namespace Frmwrk\Controllers;

use Psr\Http\Message\ResponseInterface;

/**
 * Class JsonController
 * @package Frmwrk\Controllers
 */
class JsonController
{
    /**
     * Renders a json formatted body using $data for the $response
     * @param ResponseInterface $response
     * @param array $data
     * @return ResponseInterface
     */
    protected function render(ResponseInterface $response, array $data = [])
    {
        $response->getBody()->write(json_encode($data));
        return $response;
    }
}