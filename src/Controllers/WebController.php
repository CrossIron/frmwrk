<?php

namespace Frmwrk\Controllers;

use Frmwrk\Layouts\Layout;
use League\Plates\Engine;
use Psr\Http\Message\ResponseInterface;

/**
 * Class WebController
 * @package Frmwrk\Controllers
 */
abstract class WebController
{
    /**
     * The templates engine for rendering
     * @var Engine
     */
    protected $templates;

    /**
     * The layout configuration for the controller
     * @var Layout
     */
    protected $layout;

    /**
     * HomeController constructor.
     * @param Engine $templates
     */
    public function __construct(Engine $templates)
    {
        $this->templates = $templates;
        $this->prepare();
    }

    /**
     * Prepares the controller. Always called on instantiation
     */
    protected abstract function prepare();

    /**
     * Renders a specific template with $name using $data for the $response
     * @param ResponseInterface $response
     * @param string $name
     * @param array $data
     * @return ResponseInterface
     */
    protected function render(ResponseInterface $response, string $name, array $data = [])
    {
        $template = $this->templates->make($name);
        if ($this->layout) {
            $template->layout($this->layout->getName(), $this->layout->getData());
        }
        $content = $template->render($data);
        $response->getBody()->write($content);
        return $response;
    }
}