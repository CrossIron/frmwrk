<?php

namespace Frmwrk\Controllers;

use Frmwrk\Layouts\MainLayout;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class HomeController extends WebController
{
    /**
     * Action for hello world
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @return ResponseInterface
     */
    public function greet(ServerRequestInterface $request, ResponseInterface $response)
    {
        return $this->render($response, 'hello');
    }

    public function greetBack(ServerRequestInterface $request, ResponseInterface $response)
    {
        $body = $request->getParsedBody();
        return $this->render($response, 'hello', [
            'name' => $body['name']
        ]);
    }

    /**
     * {@inheritdoc}
     */
    protected function prepare()
    {
        $this->layout = new MainLayout([
            'pageTitle' => 'Home'
        ]);
    }
}
