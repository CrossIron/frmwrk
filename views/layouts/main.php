<?php
/**
 * @var $pageTitle String
 */
?>
<html>
<head>
    <title><?php echo $pageTitle; ?></title>
</head>
<body>

<?php echo $this->section('content') ?>

</body>
</html>
