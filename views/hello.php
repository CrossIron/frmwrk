<?php
/**
 * @var $this League\Plates\Template\Template
 */
?>

<?php if (isset($name) && $name): ?>
    <h1>Why hello there <?php echo $this->e($name); ?></h1>
<?php else: ?>
    <h1>Hello world!</h1>
    <form method="post">
        <label for="name">What is your name?</label>
        <input type="text" name="name" id="name">
    </form>
<?php endif; ?>

