<?php
// instantiate the sexy di container
$container = new \League\Container\Container();

// activate auto-wiring
$container->delegate(
    new \League\Container\ReflectionContainer()
);

// share the response and request interface
$container->share('response', \Zend\Diactoros\Response::class);
$container->share('request', function () {
    return \Zend\Diactoros\ServerRequestFactory::fromGlobals(
        $_SERVER, $_GET, $_POST, $_COOKIE, $_FILES
    );
});

$container->share('emitter', Zend\Diactoros\Response\SapiEmitter::class);


return $container;