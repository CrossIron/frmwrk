<?php

$route = new \League\Route\RouteCollection($container);

$route->get('/', '\Frmwrk\Controllers\HomeController::greet');
$route->post('/', '\Frmwrk\Controllers\HomeController::greetBack');

return $route;