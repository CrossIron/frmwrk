<?php

include_once '../vendor/autoload.php';

(new \Dotenv\Dotenv(__DIR__ . '/../'))->safeLoad();

$container = require __DIR__ . '/container.php';

require_once __DIR__ . '/views.php';
require_once __DIR__ . '/database.php';

$route = require __DIR__ . '/routes.php';

$response = $route->dispatch($container->get('request'), $container->get('response'));

$container->get('emitter')->emit($response);