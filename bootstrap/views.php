<?php

// Views
$container->add(\League\Plates\Engine::class, function () {
    $basePath = __DIR__ . '/../views';
    $templates = new League\Plates\Engine($basePath);
    $templates->addFolder('layout', $basePath . '/layouts');
    return $templates;
});